import React from "react";
import AppRouting from "components/pages/AppRouting";
import { BaseApp } from "./style";

function App() {
    return (
        <BaseApp>
            <AppRouting />
        </BaseApp>
    );
}

export default App;
