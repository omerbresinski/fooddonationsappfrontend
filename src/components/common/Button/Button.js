import React from "react";
import Text from "components/common/Text";
import Spinner from "components/common/Spinner";
import { BaseButton, ButtonWrapper } from "./style";

const Button = (props) => {
    return (
        <ButtonWrapper>
            {props.isLoading ? (
                <Spinner />
            ) : (
                <BaseButton onClick={props.onClick}>
                    <Text size={props.size} hoverColor={props.hoverColor}>
                        {props.value}
                    </Text>
                </BaseButton>
            )}
        </ButtonWrapper>
    );
};

export default React.memo(Button);
