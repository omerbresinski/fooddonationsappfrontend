import styled from "styled-components";

export const BaseButton = styled.div`
    display: flex;
    box-sizing: border-box;
    height: auto;
    border: 3px solid darkgray;
    border-radius: 8px;
    align-items: center;
    padding: 7px 14px;
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    &:hover {
        border-color: darkslateblue;
    }
`;

export const ButtonWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 40px;
`;
