import React, { useState, useEffect, useRef, useCallback } from "react";
import { createSelector } from "reselect";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { getAllItems, setSelectedItem } from "store/actions";
import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import { BaseMap, MapHeader, MapWrapper, mapStyles } from "./style";
import { API_KEY, URL } from "constant";
import { useUser } from "hooks";
import Text from "components/common/Text";
import Spinner from "components/common/Spinner";
import markerIcon from "icons/map_marker.png";

const Map = (props) => {
    const mapRef = useRef();
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const mapPreview = useSelector((state) => state.mapPreview);
    const digitalItems = useSelector(selectDigitalItems);
    const [startingPosition, setStartingPosition] = useState();
    const { isLoaded } = useLoadScript({ googleMapsApiKey: API_KEY, libraries: ["places, geocoding"] });

    useEffect(() => {
        initializeStartingPosition();
    }, []);

    useEffect(() => {
        if (mapPreview?.length) {
            panTo(mapPreview[0]);
        }
    }, [mapPreview]);

    useEffect(() => {
        if (user) {
            dispatch(getAllItems(user.userId?.email));
        }
    }, [user]);

    const initializeStartingPosition = () => {
        navigator.geolocation.getCurrentPosition(({ coords }) => {
            setStartingPosition({ lat: coords.latitude, lng: coords.longitude });
        });
    };

    const handleItemClick = (item) => {
        panTo(item.location);
        dispatch(setSelectedItem(item));
        history.push(URL.items);
    };

    const panTo = (location) => {
        mapRef.current?.panTo(location);
        mapRef.current?.setZoom(18.2);
    };

    const onMapLoad = useCallback((map) => (mapRef.current = map), []);

    return startingPosition && isLoaded ? (
        <BaseMap height={props.height} width={props.width}>
            <MapHeader>
                <Text size={"72px"} bold withStroke color={"darkslateblue"}>
                    Foodono
                </Text>
            </MapHeader>
            <MapWrapper isBlurred={!user}>
                <GoogleMap
                    mapContainerStyle={{ width: "100%", height: "100%" }}
                    center={startingPosition}
                    zoom={12}
                    onLoad={onMapLoad}
                    options={{
                        mapStyles,
                        disableDefaultUI: true,
                    }}
                >
                    {mapPreview?.length && (
                        <Marker position={{ lat: mapPreview[0].lat, lng: mapPreview[0].lng }} title={"New Food Provider"} icon={markerIcon} />
                    )}
                    {user &&
                        digitalItems
                            ?.filter(({ active }) => active)
                            ?.map((item) => {
                                return (
                                    <Marker
                                        key={item.itemId.id}
                                        position={{ lat: item.location.lat, lng: item.location.lng }}
                                        title={item.name}
                                        onClick={() => handleItemClick(item)}
                                        icon={markerIcon}
                                    />
                                );
                            })}
                </GoogleMap>
            </MapWrapper>
        </BaseMap>
    ) : (
        <Spinner />
    );
};

const getDigitalItems = (state) => {
    return state.items;
};
const selectDigitalItems = createSelector([getDigitalItems], (items) => items);

export default React.memo(Map);
