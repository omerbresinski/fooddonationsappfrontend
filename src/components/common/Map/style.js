import styled from "styled-components";

export const BaseMap = styled.div`
    display: flex;
    position: relative;
    box-sizing: border-box;
    width: ${({ width }) => width || "100%"};
    height: ${({ height }) => height || "100%"};
    transition: all 0.2s ease-in-out;
`;

export const MapHeader = styled.div`
    display: flex;
    position: absolute;
    top: 0px;
    margin-block-start: 10px;
    margin-inline-start: 15px;
    z-index: 999;
`;

export const MapWrapper = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    ${({ isBlurred }) => isBlurred && `filter: blur(3px);`}
    ${({ isBlurred }) => isBlurred && `-webkit-filter: blur(3px);`}
    transition: all 0.3s ease-in-out;
    outline: 2px solid lightgray;
`;

export const mapStyles = [
    {
        featureType: "administrative",
        elementType: "all",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "administrative",
        elementType: "labels",
        stylers: [
            {
                visibility: "on",
            },
            {
                color: "#716464",
            },
            {
                weight: "0.01",
            },
        ],
    },
    {
        featureType: "administrative.country",
        elementType: "labels",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "landscape",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "landscape.natural",
        elementType: "geometry",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "landscape.natural.landcover",
        elementType: "geometry",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "geometry.fill",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "geometry.stroke",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "labels.text",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "labels.text.fill",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "labels.text.stroke",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "poi.attraction",
        elementType: "geometry",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "all",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [
            {
                visibility: "simplified",
            },
            {
                color: "#a05519",
            },
            {
                saturation: "-13",
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "transit",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "transit",
        elementType: "geometry",
        stylers: [
            {
                visibility: "simplified",
            },
        ],
    },
    {
        featureType: "transit.station",
        elementType: "geometry",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "water",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified",
            },
            {
                color: "#84afa3",
            },
            {
                lightness: 52,
            },
        ],
    },
    {
        featureType: "water",
        elementType: "geometry",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
];
