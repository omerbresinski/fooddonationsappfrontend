import styled from "styled-components";

export const BaseOperationItem = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px 10px;
    border: 1px solid blueviolet;
    border-radius: 30px;
    box-sizing: border-box;
`;
