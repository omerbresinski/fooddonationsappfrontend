import React from "react";
import usePlacesAutocomplete, { getGeocode } from "use-places-autocomplete";
import Text from "components/common/Text";
import TextInput from "components/common/TextInput";
import { BasePlacesSearchInput, OptionsList, Option } from "./style";

const PlacesSearchInput = (props) => {
    const {
        value,
        suggestions: { status, data },
        setValue,
        clearSuggestions,
    } = usePlacesAutocomplete({
        requestOptions: {
            location: { lat: () => 32.0660485, lng: () => 34.8292901 },
            radius: 200 * 1000,
        },
    });

    const handleSearchChange = (_, searchValue) => {
        setValue(searchValue);
    };

    const handleSelectSuggestion = async (address) => {
        clearSuggestions();
        const geoCode = await getGeocode({ address: address.description });
        const lat = geoCode[0]?.geometry?.location?.lat();
        const lng = geoCode[0]?.geometry?.location?.lng();
        props.onChange({ lat, lng });
    };

    return (
        <BasePlacesSearchInput>
            <TextInput fieldName={"address"} label={"Address"} onChange={handleSearchChange} width={"400px"} value={value || ""} />
            {data?.length ? (
                <OptionsList>
                    {status === "OK" &&
                        data.map((suggestion) => (
                            <Option
                                key={suggestion.place_id}
                                onClick={() => {
                                    handleSelectSuggestion(suggestion);
                                }}
                            >
                                <Text size="24px">{suggestion.description}</Text>
                            </Option>
                        ))}
                </OptionsList>
            ) : (
                <></>
            )}
        </BasePlacesSearchInput>
    );
};

export default React.memo(PlacesSearchInput);
