import styled from "styled-components";
import { BaseTextInput } from "components/common/TextInput/style";

export const OptionsList = styled.div`
    display: flex;
    position: absolute;
    flex-direction: column;
    width: 100%;
    border: 2px solid darkgray;
    padding-top: 7px;
    border-top: none;
    top: 80px;
    background: white;
    left: 0px;
    z-index: 999;
    transition: all 0.2s ease-in-out;
    overflow: hidden;
    border-bottom-left-radius: 7px;
    border-bottom-right-radius: 7px;
    box-sizing: border-box;
`;

export const BasePlacesSearchInput = styled.div`
    display: flex;
    position: relative;
    &:hover ${OptionsList} {
        border-color: darkslateblue;
    }
    &:hover ${BaseTextInput} {
        border-color: darkslateblue;
    }
`;

export const Option = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    padding: 5px 14px;
    color: darkgray;
    transition: all 0.15s ease-in-out;
    cursor: pointer;
    &:hover {
        color: darkslateblue;
    }
`;
