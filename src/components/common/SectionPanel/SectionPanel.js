import React from "react";
import Text from "components/common/Text";
import { CreateOperation, EditOperation, DeleteOperation } from "components/features/Operations";
import { BaseSectionPanel, Header, Content } from "./style";

const SectionPanel = (props) => {
    const isActive = props.index === props.activeIndex;

    const handleSectionClick = () => {
        props.onClick(props.index);
    };

    return (
        <BaseSectionPanel>
            <Header onClick={handleSectionClick}>
                <Text size="22px" color={isActive ? "darkslateblue" : "darkgray"}>
                    {props.label}
                </Text>
            </Header>
            {isActive && (
                <Content>
                    {props.type === "create" && <CreateOperation provider={props.provider} />}
                    {props.type === "edit" && <EditOperation provider={props.provider} />}
                    {props.type === "delete" && <DeleteOperation provider={props.provider} />}
                </Content>
            )}
        </BaseSectionPanel>
    );
};

export default React.memo(SectionPanel);
