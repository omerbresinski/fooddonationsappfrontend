import styled from "styled-components";
import { BaseText } from "components/common/Text/style";

export const BaseSectionPanel = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    min-height: 50px;
    height: auto;
    width: 100%;
    box-sizing: border-box;
`;

export const Header = styled.div`
    display: block;
    justify-content: flex-start;
    height: 100%;
    align-items: center;
    padding-block-start: 10px;
    padding-inline-start: 10px;
    box-sizing: border-box;
    cursor: pointer;
    &:hover ${BaseText} {
        color: darkslateblue;
    }
`;

export const Content = styled.div`
    display: flex;
    padding: 10px 8px;
`;
