import React, { useState } from "react";
import Text from "components/common/Text";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { BaseSelectInput, LabelAndInput, OptionsList, Option, SelectedValue } from "./style";

const SelectInput = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const selectedValue = props.options?.find((option) => option.value === props.value);

    const handleChange = (option) => {
        props.onChange && props.onChange(props.fieldName, option.value);
    };

    const handleSelectClick = () => {
        setIsOpen((isOpen) => !isOpen);
    };

    return (
        <LabelAndInput>
            <Text size={"22px"} color={"#606060"}>
                {props.label}
            </Text>
            <BaseSelectInput value={props.value} width={props.width} size={props.size} onClick={handleSelectClick}>
                <SelectedValue>{selectedValue?.label}</SelectedValue>
                <FontAwesomeIcon color={"lightgray"} icon={faCaretDown} className="icon" />
                {isOpen && props.options?.length ? (
                    <OptionsList size={props.size}>
                        {props.options.map((option) => (
                            <Option key={option.value} onClick={() => handleChange(option)}>
                                {option.label}
                            </Option>
                        ))}
                    </OptionsList>
                ) : (
                    <></>
                )}
            </BaseSelectInput>
        </LabelAndInput>
    );
};

export default React.memo(SelectInput);
