import styled from "styled-components";

export const OptionsList = styled.div`
    display: flex;
    position: absolute;
    flex-direction: column;
    width: 100%;
    border: 2px solid darkgray;
    padding-top: 7px;
    border-top: none;
    top: ${({ size }) => (size === "small" ? "30px" : "53px")};
    background: white;
    left: -2px;
    z-index: 999;
    transition: all 0.2s ease-in-out;
    border-bottom-left-radius: 7px;
    border-bottom-right-radius: 7px;
`;

export const BaseSelectInput = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
    box-sizing: border-box;
    height: ${({ size }) => (size === "small" ? "40px" : "60px")};
    width: ${({ width }) => width || `250px`};
    max-width: ${({ width }) => width || `250px`};
    padding: 10px 14px;
    border: 2px solid darkgray;
    transition: all 0.2s ease-in-out;
    border-radius: 7px;
    font-size: ${({ size }) => (size === "small" ? "18px" : "34px")};
    font-family: Calibri, Tahoma, Arial;
    color: #606060;
    cursor: pointer;
    .icon {
        transition: all 0.2s ease-in-out;
    }
    &:hover {
        color: darkslateblue;
        border-color: darkslateblue;
        .icon {
            color: darkslateblue;
        }
    }
    &:hover ${OptionsList} {
        border-color: darkslateblue;
    }
    &:focus {
        color: darkslateblue;
        outline: none;
        border-color: darkslateblue;
    }
`;

export const LabelAndInput = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 14px;
    gap: 2px;
`;

export const Option = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    padding: 5px 14px;
    color: darkgray;
    transition: all 0.15s ease-in-out;
    &:hover {
        color: darkslateblue;
    }
`;

export const SelectedValue = styled.div`
    display: flex;
`;
