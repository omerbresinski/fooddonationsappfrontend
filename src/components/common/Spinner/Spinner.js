import React from "react";
import { BaseSpinner } from "./style";

const Spinner = ({ width, height, thickness }) => {
    return <BaseSpinner width={width} height={height} thickness={thickness} />;
};

export default React.memo(Spinner);
