import styled, { keyframes } from "styled-components";

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(-360deg);
  }
`;

export const BaseSpinner = styled.div`
    animation: ${rotate360} 1s linear infinite;
    transform: translateZ(0);

    border-top: ${({ thickness }) => thickness || "2px"} solid grey;
    border-right: ${({ thickness }) => thickness || "2px"} solid grey;
    border-bottom: ${({ thickness }) => thickness || "2px"} solid grey;
    border-left: ${({ thickness }) => thickness || "2px"} solid black;
    background: transparent;
    width: ${({ width }) => width || "24px"};
    height: ${({ height }) => height || "24px"};
    border-radius: 50%;
`;
