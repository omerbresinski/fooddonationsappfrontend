import React from "react";
import Text from "components/common/Text";
import { BaseTab } from "./style";

const Tab = (props) => {
    const handleClick = () => {
        props.onClick(props.option);
    };

    return (
        <BaseTab onClick={handleClick}>
            <Text size="26px">{props.option}</Text>
        </BaseTab>
    );
};

export default React.memo(Tab);
