import styled from "styled-components";

export const BaseTab = styled.div`
    display: flex;
    height: 50px;
    padding: 0px 10px 3px;
    justify-content: center;
    align-items: flex-end;
    width: auto;
    box-sizing: border-box;
    border: 3px solid black;
    border-bottom: none;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    margin-inline-end: -3px;
    transition: all 0.15s ease-in-out;

    &:hover {
        border-color: blueviolet;
        color: blueviolet;
        cursor: pointer;
    }
`;
