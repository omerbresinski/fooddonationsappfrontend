import React from "react";
import Tab from "components/common/Tab";
import { BaseTabs } from "./style";

const Tabs = (props) => {
    return (
        <BaseTabs>
            {props.options.map((option, index) => (
                <Tab key={index} option={option} onClick={props.onTabClick} />
            ))}
        </BaseTabs>
    );
};

export default React.memo(Tabs);
