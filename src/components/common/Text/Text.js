import React from "react";
import { BaseText } from "./style";

const Text = (props) => {
    return (
        <BaseText
            size={props.size}
            color={props.color}
            weight={props.bold}
            italic={props.italic}
            withStroke={props.withStroke}
            isLink={props.onClick}
            hoverColor={props.hoverColor}
            onClick={props.onClick}
        >
            {props.children}
        </BaseText>
    );
};

export default Text;
