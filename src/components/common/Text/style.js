import styled from "styled-components";

export const BaseText = styled.span`
    color: ${({ color }) => color || "darkslateblue"};
    font-size: ${({ size }) => size};
    font-family: Calibri, Tahoma, Arial;
    font-weight: ${({ weight }) => (weight ? "bold" : "normal")};
    font-style: ${({ italic }) => italic && "italic"};
    ${({ withStroke, color }) => (withStroke ? ` -webkit-text-stroke-width: 2px; -webkit-text-stroke-color: ${color};` : "")};
    &:hover {
        ${({ isLink }) => isLink && `cursor: pointer;`}
        ${({ isLink }) => isLink && `color: darkslateblue;`}
        ${({ hoverColor }) => hoverColor && `color: ${hoverColor};`}
    }
    transition: all 0.1s ease-in-out;
`;
