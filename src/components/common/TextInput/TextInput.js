import React from "react";
import Text from "components/common/Text";
import { BaseTextInput, LabelAndInput } from "./style";

const TextInput = (props) => {
    const handleChange = (e) => {
        props.onChange && props.onChange(props.fieldName, e.target.value);
    };

    return (
        <LabelAndInput>
            <Text size={props.size === "small" ? "16px" : "22px"} color={"#606060"}>
                {props.label}
            </Text>
            <BaseTextInput value={props.value} width={props.width} onChange={handleChange} size={props.size} />
        </LabelAndInput>
    );
};

export default React.memo(TextInput);
