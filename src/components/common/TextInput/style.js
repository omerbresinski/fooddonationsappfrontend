import styled from "styled-components";

export const BaseTextInput = styled.input`
    height: ${({ size }) => (size === "small" ? "40px" : "60px")};
    width: ${({ width }) => width || `250px`};
    max-width: ${({ width }) => width || `250px`};
    box-sizing: border-box;
    padding: ${({ size }) => (size === "small" ? "7px 10px" : "10px 14px")};
    border: 2px solid darkgray;
    transition: all 0.2s ease-in-out;
    border-radius: 7px;
    font-size: ${({ size }) => (size === "small" ? "18px" : "34px")};
    font-family: Calibri, Tahoma, Arial;
    color: #606060;
    &:hover {
        color: darkslateblue;
        border-color: darkslateblue;
    }
    &:focus {
        color: darkslateblue;
        outline: none;
        border-color: darkslateblue;
    }
`;

export const LabelAndInput = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 14px;
    gap: 2px;
`;
