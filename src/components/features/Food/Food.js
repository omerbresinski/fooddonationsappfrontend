import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { setFoodDelivered } from "store/actions";
import { useForm, useUser } from "hooks";
import Text from "components/common/Text";
import TextInput from "components/common/TextInput";
import { BaseFood, Description, Form, Inputs } from "./style";
import Button from "components/common/Button";
import { URL } from "constant";

const Food = (props) => {
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const { form, handleInputChange } = useForm();

    const handleFormClick = (e) => {
        e.stopPropagation();
    };

    const handleSubmitClick = () => {
        props.onSubmit(form.foodName, form.amount);
    };

    const handleCloseOrderClick = () => {
        const deliveredRequest = {
            type: "food_delivered",
            item: { itemId: { ...props.item.itemId } },
            invokedBy: { userId: user.userId },
            operationAttributes: {
                name: props.reservationName,
            },
        };
        dispatch(setFoodDelivered(deliveredRequest, onDeliveredSuccess));
    };

    const onDeliveredSuccess = () => {
        history.push(URL.home);
    };

    return (
        <BaseFood>
            <Description>
                <Text size="22px">{props.type}</Text>
                <Text size="22px">-</Text>
                <Text size="22px">{props.numberOfDishes}</Text>
            </Description>
            {props.isFormVisible &&
                (props.recievers?.length ? (
                    <Button value="Close Order" onClick={handleCloseOrderClick} />
                ) : (
                    <Form onClick={handleFormClick}>
                        <Inputs>
                            <TextInput label="Food Name" size="small" value={form.foodName || ""} onChange={handleInputChange} fieldName="foodName" />
                            <TextInput label="Amount" size="small" value={form.amount || ""} onChange={handleInputChange} fieldName="amount" />
                        </Inputs>
                        <Button value="Schedule Pickup" onClick={handleSubmitClick} />
                    </Form>
                ))}
        </BaseFood>
    );
};

export default React.memo(Food);
