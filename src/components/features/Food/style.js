import styled from "styled-components";

export const BaseFood = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`;

export const Description = styled.div`
    display: flex;
    gap: 7px;
`;

export const Form = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    gap: 15px;
    cursor: initial;
`;

export const Inputs = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    gap: 15px;
`;
