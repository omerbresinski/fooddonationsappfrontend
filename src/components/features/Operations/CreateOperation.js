import React from "react";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { createOperation } from "store/actions";
import { useForm, useUser } from "hooks";
import moment from "moment";
import { ISRAEL_UTC_OFFSET, URL } from "constant";
import Button from "components/common/Button";
import TextInput from "components/common/TextInput";
import { BaseOperation, InputRow, ButtonWrapper } from "./style";

const CreateOperation = (props) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useUser();
    const { form, handleInputChange } = useForm();

    const onCreateSuccess = () => {
        history.push(URL.home);
    };

    const onCreateClick = () => {
        const startDate = moment(`${form.startDate} ${form.startTime}`);
        const endDate = moment(`${form.endDate} ${form.endTime}`);

        const createRequest = {
            type: "create_pickup_window",
            item: { itemId: { ...props.provider.itemId } },
            invokedBy: { userId: user.userId },
            operationAttributes: {
                reservasionsDetails: [
                    {
                        name: form.name,
                        pickupTime: {
                            start: startDate.add(ISRAEL_UTC_OFFSET, "hours").toISOString(),
                            end: endDate.add(ISRAEL_UTC_OFFSET, "hours").toISOString(),
                        },
                        pickupLocation: { ...props.provider.location },
                        foods: [{ type: form.foodType, numberOfDishes: form.numberOfDishes }],
                    },
                ],
            },
        };
        dispatch(createOperation(createRequest, onCreateSuccess));
    };

    return (
        <BaseOperation>
            <InputRow>
                <TextInput fieldName={"name"} label={"Name"} onChange={handleInputChange} width={"775px"} size="small" value={form.name || ""} />
            </InputRow>
            <InputRow>
                <TextInput
                    fieldName={"startDate"}
                    label={"Starting Date"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.startDate || ""}
                />
                <TextInput
                    fieldName={"startTime"}
                    label={"Starting Time"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.startTime || ""}
                />
            </InputRow>
            <InputRow>
                <TextInput
                    fieldName={"endDate"}
                    label={"Ending Date"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.endDate || ""}
                />
                <TextInput
                    fieldName={"endTime"}
                    label={"Ending Time"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.endTime || ""}
                />
            </InputRow>
            <InputRow>
                <TextInput
                    fieldName={"foodType"}
                    label={"Food Type"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.foodType || ""}
                />
                <TextInput
                    fieldName={"numberOfDishes"}
                    label={"Amount"}
                    onChange={handleInputChange}
                    width={"375px"}
                    size="small"
                    value={form.numberOfDishes || ""}
                />
            </InputRow>
            <InputRow>
                <ButtonWrapper>
                    <Button onClick={onCreateClick} value={"Create Pickup Window"} size={"24px"} hoverColor={"darkslateblue"} />
                </ButtonWrapper>
            </InputRow>
        </BaseOperation>
    );
};

export default React.memo(CreateOperation);
