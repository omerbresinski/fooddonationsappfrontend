import React from "react";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { createOperation } from "store/actions";
import { useForm, useUser } from "hooks";
import { URL } from "constant";
import Button from "components/common/Button";
import { BaseOperation, InputRow, ButtonWrapper } from "./style";
import SelectInput from "components/common/SelectInput";

const DeleteOperation = (props) => {
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const { form, handleInputChange } = useForm();
    const pickupWindows = props.provider?.itemAttributes?.reservasionsDetails?.map(({ name }) => {
        return { value: name, label: name };
    });

    const onCreateSuccess = () => {
        history.push(URL.home);
    };

    const onDeleteClick = () => {
        const closePickupWindowRequest = {
            type: "cancel_pickup_window",
            item: { itemId: { ...props.provider.itemId } },
            invokedBy: { userId: user.userId },
            operationAttributes: { reservasionsDetails: [{ name: form.name }] },
        };
        dispatch(createOperation(closePickupWindowRequest, onCreateSuccess));
    };

    return (
        <BaseOperation>
            <InputRow>
                <SelectInput
                    fieldName={"name"}
                    label={"Select Pickup Window"}
                    onChange={handleInputChange}
                    width={"400px"}
                    value={form.name}
                    options={pickupWindows}
                    size="small"
                />
            </InputRow>
            <InputRow>
                <ButtonWrapper>
                    <Button onClick={onDeleteClick} value={"Delete Pickup Window"} size={"24px"} hoverColor={"darkslateblue"} />
                </ButtonWrapper>
            </InputRow>
        </BaseOperation>
    );
};

export default React.memo(DeleteOperation);
