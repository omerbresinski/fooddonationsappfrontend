import styled from "styled-components";

export const BaseOperation = styled.div`
    display: flex;
    flex-direction: column;
    padding: 7px 20px;
    box-sizing: border-box;
    gap: 15px;
`;

export const InputRow = styled.div`
    display: flex;
    width: 100%;
    gap: 25px;
`;

export const ButtonWrapper = styled.div`
    display: flex;
    margin: 10px 0px;
`;
