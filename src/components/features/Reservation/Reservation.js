import React, { useState } from "react";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { schedulePickup } from "store/actions";
import { useUser } from "hooks";
import moment from "moment";
import { DATE_FORMAT, URL } from "constant";
import Text from "components/common/Text";
import Food from "components/features/Food";
import { BaseReservation, Header, Content, Date, ButtonWrapper } from "./style";

const Reservation = (props) => {
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const [isFormVisible, setIsFormVisible] = useState(false);

    const handleReservationClick = () => {
        setIsFormVisible((isVisible) => !isVisible);
    };

    const handleSubmitClick = (type, numberOfDishes) => {
        const scheduleRequest = {
            type: "schedule_pickup",
            item: { itemId: { ...props.item.itemId } },
            invokedBy: { userId: user.userId },
            operationAttributes: {
                name: props.name,
                pickupTime: { ...props.pickupTime },
                foodsToTake: [{ type, numberOfDishes }],
            },
        };
        dispatch(schedulePickup(scheduleRequest, onScheduleSuccess));
    };

    const onScheduleSuccess = () => {
        history.push(URL.home);
    };

    return (
        <BaseReservation onClick={handleReservationClick}>
            <Header>
                <Text size="28px">{props.name}</Text>
            </Header>
            <Content>
                <Text size="22px">Available from</Text>
                <Date>
                    <Text size="22px">{moment(props.pickupTime.start).format(DATE_FORMAT)}</Text>
                    <Text size="22px">{`to`}</Text>
                    <Text size="22px">{moment(props.pickupTime.end).format(DATE_FORMAT)}</Text>
                </Date>
                <Text size="22px">Food:</Text>
                {props.food?.map(({ type, numberOfDishes }, index) => (
                    <Food
                        key={index}
                        reservationName={props.name}
                        type={type}
                        numberOfDishes={numberOfDishes}
                        isFormVisible={isFormVisible}
                        onSubmit={handleSubmitClick}
                        recievers={props.recievers}
                        item={props.item}
                    />
                ))}
                <ButtonWrapper></ButtonWrapper>
            </Content>
        </BaseReservation>
    );
};

export default React.memo(Reservation);
