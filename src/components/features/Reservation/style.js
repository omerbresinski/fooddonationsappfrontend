import styled from "styled-components";

export const BaseReservation = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    border: 2px solid darkgray;
    border-radius: 7px;
    padding: 10px;
    transition: all 0.15s ease-in-out;
    color: darkgray;
    &:hover {
        cursor: pointer;
        border-color: darkslateblue;
    }
`;

export const Header = styled.div`
    display: flex;
    margin-block-end: 10px;
`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    gap: 5px;
`;

export const Date = styled.div`
    display: flex;
    gap: 7px;
`;

export const ButtonWrapper = styled.div`
    display: flex;
`;
