import React from "react";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { logout } from "store/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt, faSignOutAlt, faUserPlus, faCalendarAlt, faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { useUser } from "hooks";
import { BaseSideMenu, TopMenu, BottomMenu, Placeholder, IconWrapper } from "./style";
import { URL } from "constant";

const SideMenu = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useUser();

    const handleLogoutClick = () => {
        dispatch(logout());
    };

    const handleAddFoodProviderClick = () => {
        history.push(URL.restaurantCreate);
    };

    const handleOperationsClick = () => {
        history.push(URL.operations);
    };

    const handleLoginClick = () => {
        history.push(URL.login);
    };

    const handleRegisterClick = () => {
        history.push(URL.register);
    };

    return (
        <BaseSideMenu>
            {user ? (
                <TopMenu>
                    <IconWrapper title="Add Food Provider" onClick={handleAddFoodProviderClick}>
                        <FontAwesomeIcon size={"2x"} color={"lightgray"} icon={faAppleAlt} className="icon" />
                    </IconWrapper>
                    <IconWrapper title="Operations" onClick={handleOperationsClick}>
                        <FontAwesomeIcon size={"2x"} color={"lightgray"} icon={faCalendarAlt} className="icon" />
                    </IconWrapper>
                </TopMenu>
            ) : (
                <Placeholder />
            )}
            <BottomMenu>
                {user ? (
                    <IconWrapper title="Logout" onClick={handleLogoutClick}>
                        <FontAwesomeIcon size={"2x"} color={"lightgray"} icon={faSignOutAlt} className="icon" />
                    </IconWrapper>
                ) : (
                    <>
                        <IconWrapper title="Login" onClick={handleLoginClick}>
                            <FontAwesomeIcon size={"2x"} color={"lightgray"} icon={faSignInAlt} className="icon" />
                        </IconWrapper>
                        <IconWrapper title="Register" onClick={handleRegisterClick}>
                            <FontAwesomeIcon size={"2x"} color={"lightgray"} icon={faUserPlus} className="icon" />
                        </IconWrapper>
                    </>
                )}
            </BottomMenu>
        </BaseSideMenu>
    );
};

export default SideMenu;
