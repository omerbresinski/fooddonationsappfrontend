import styled from "styled-components";

export const BaseSideMenu = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 80px;
    min-width: 80px;
    height: 100%;
    justify-content: space-between;
    align-items: center;
    padding: 20px 7px;
    background: darkslateblue;
    box-sizing: border-box;
`;

export const TopMenu = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
`;

export const BottomMenu = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
`;

export const Placeholder = styled.div`
    display: flex;
`;

export const IconWrapper = styled.div`
    display: flex;
    justify-content: center;
    &:hover {
        cursor: pointer;
        .icon {
            color: white;
        }
    }
`;
