import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { URL } from "constant";
import Map from "components/common/Map";
import SideMenu from "components/features/SideMenu";
import { Login, Register, Home, Operations, RestaurantCreate, Items } from "components/pages";
import { MapWrapper } from "./style";

const AppRouting = () => {
    return (
        <Router>
            <MapWrapper>
                <Map height={"100vh"} width={"100%"} />
            </MapWrapper>
            <Switch>
                <Route path={URL.home} exact>
                    <Home />
                </Route>
                <Route path={URL.login} exact>
                    <Login />
                </Route>
                <Route path={URL.items} exact>
                    <Items />
                </Route>
                <Route path={URL.register} exact>
                    <Register />
                </Route>
                <Route path={URL.operations} exact>
                    <Operations />
                </Route>
                <Route path={URL.restaurantCreate} exact>
                    <RestaurantCreate />
                </Route>
            </Switch>
            <SideMenu />
        </Router>
    );
};

export default AppRouting;
