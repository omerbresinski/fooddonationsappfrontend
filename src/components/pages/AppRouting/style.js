import styled from "styled-components";

export const MapWrapper = styled.div`
    display: flex;
    width: 100%;
    transition: all 0.2s ease-in-out;
`;
