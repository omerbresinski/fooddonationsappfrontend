import React from "react";
import { useSelector } from "react-redux";
import { withAuth } from "hooks";

const Home = () => {
    const state = useSelector((state) => state);
    return <></>;
};

export default withAuth(Home);
