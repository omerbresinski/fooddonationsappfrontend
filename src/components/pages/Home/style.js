import styled from "styled-components";

export const BaseHome = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    padding: 20px;
    box-sizing: border-box;
`;
