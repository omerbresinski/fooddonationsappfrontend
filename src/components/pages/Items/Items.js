import React from "react";
import { withAuth } from "hooks";
import { useSelector } from "react-redux";
import { createSelector } from "reselect";
import Text from "components/common/Text";
import Reservation from "components/features/Reservation";
import { BaseItems, Header, Content, ActivePickupWindows } from "./style";

const Items = () => {
    const selectedItem = useSelector(selectSelectedItem);
    const reservationDetails = selectedItem?.itemAttributes?.reservasionsDetails;

    return (
        <BaseItems>
            <Header>
                <Text size="52px" withSroke>
                    {selectedItem?.name}
                </Text>
            </Header>
            <Content>
                <Text size="30px">Active Pickup Windows</Text>
                <ActivePickupWindows>
                    {reservationDetails?.length ? (
                        reservationDetails?.map(({ name, pickupTime, pickupLocation, foodsToGive, recievers }) => (
                            <Reservation
                                key={name}
                                name={name}
                                pickupTime={pickupTime}
                                pickupLocation={pickupLocation}
                                food={foodsToGive}
                                recievers={recievers}
                                item={selectedItem}
                            />
                        ))
                    ) : (
                        <Text size="18px" color="darkgray" italic>
                            There are no currently active pickup windows for this provider
                        </Text>
                    )}
                </ActivePickupWindows>
            </Content>
        </BaseItems>
    );
};

const getItems = (state) => state.items;
const selectSelectedItem = createSelector([getItems], (items) => items?.find((item) => item.isSelected));

export default withAuth(Items);
