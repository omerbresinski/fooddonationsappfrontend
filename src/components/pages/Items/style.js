import styled from "styled-components";

export const BaseItems = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    padding: 50px;
    box-sizing: border-box;
`;

export const Header = styled.div`
    display: flex;
    margin-block-end: 30px;
`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px 3px;
`;

export const ActivePickupWindows = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-block-start: 30px;
    gap: 10px;
`;
