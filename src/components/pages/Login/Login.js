import React from "react";
import { login } from "store/actions";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useForm } from "hooks";
import Text from "components/common/Text";
import Button from "components/common/Button";
import TextInput from "components/common/TextInput";
import { BaseLogin, Form, Header, RegisterSuggestion, ButtonWrapper } from "./style";
import { URL } from "constant";

const Login = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { form, handleInputChange } = useForm();

    const onLoginClick = () => {
        dispatch(login(form, onLoginSuccess));
    };

    const onLoginSuccess = () => {
        history.push(URL.home);
    };

    const onRegisterClick = () => {
        history.push(URL.register);
    };

    return (
        <BaseLogin>
            <Header>
                <Text size={"82px"} color={"darkslateblue"} withStroke>
                    Login
                </Text>
            </Header>
            <Form>
                <TextInput
                    fieldName={"username"}
                    label={"Username"}
                    onChange={handleInputChange}
                    width={"400px"}
                    value={form.username || ""}
                ></TextInput>
                <TextInput fieldName={"email"} label={"Email"} onChange={handleInputChange} width={"400px"} value={form.email || ""}></TextInput>
                <ButtonWrapper>
                    <Button onClick={onLoginClick} value={"Submit"} size={"32px"} hoverColor={"darkslateblue"} />
                </ButtonWrapper>
            </Form>
            <RegisterSuggestion>
                <Text size={"24px"} onClick={onRegisterClick}>
                    Create a free account
                </Text>
            </RegisterSuggestion>
        </BaseLogin>
    );
};

export default Login;
