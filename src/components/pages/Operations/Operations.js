import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateUserToPlayer } from "store/actions";
import { createSelector } from "reselect";
import { useUser, useForm, withAuth } from "hooks";
import Text from "components/common/Text";
import SelectInput from "components/common/SelectInput";
import SectionPanel from "components/common/SectionPanel";
import { BaseOperations, Header, Sections } from "./style";

const Operations = () => {
    const dispatch = useDispatch();
    const { user } = useUser();
    const { form, handleInputChange } = useForm();
    const [activeSectionIndex, setActiveSectionIndex] = useState();
    const providerList = useSelector((state) => selectProviders(state, user.userId));
    const selectedProvider = providerList?.find((provider) => provider.value === form.provider);

    useEffect(() => {
        dispatch(updateUserToPlayer(user.userId.email));
    }, []);

    const handleSectionClick = (index) => {
        setActiveSectionIndex(index);
    };

    return (
        <BaseOperations>
            <Header>
                <Text size="52px" withStroke>
                    Operations
                </Text>
            </Header>
            <SelectInput
                fieldName={"provider"}
                label={"Select Food Provider"}
                onChange={handleInputChange}
                width={"400px"}
                value={form.provider}
                options={providerList}
            />
            <Sections>
                <SectionPanel
                    type={"create"}
                    activeIndex={activeSectionIndex}
                    index={0}
                    label={"Create Pickup Window"}
                    onClick={handleSectionClick}
                    provider={selectedProvider?.data}
                />
                <SectionPanel
                    type={"edit"}
                    activeIndex={activeSectionIndex}
                    index={1}
                    label={"Edit Pickup Window"}
                    onClick={handleSectionClick}
                    provider={selectedProvider?.data}
                />
                <SectionPanel
                    type={"delete"}
                    activeIndex={activeSectionIndex}
                    index={2}
                    label={"Delete Pickup Window"}
                    onClick={handleSectionClick}
                    provider={selectedProvider?.data}
                />
            </Sections>
        </BaseOperations>
    );
};

const getUserId = (_, userId) => userId;
const getProviders = (state) => state.items;
const selectProviders = createSelector([getProviders, getUserId], (providers, { email, space }) =>
    providers
        ?.filter((provider) => provider.createdBy.userId.email === email && provider.createdBy.userId.space === space)
        ?.map((provider) => {
            return { value: provider.itemId.id, label: provider.name, data: provider };
        })
);

export default withAuth(React.memo(Operations));
