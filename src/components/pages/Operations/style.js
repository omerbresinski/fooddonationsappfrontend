import styled from "styled-components";

export const BaseOperations = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    padding: 50px;
    box-sizing: border-box;
`;

export const Header = styled.div`
    display: flex;
    margin-block-end: 30px;
`;

export const Sections = styled.div`
    display: flex;
    margin-block-start: 30px;
    flex-direction: column;
    border: 2px solid darkgray;
    border-radius: 7px;
    height: auto;
    & > * {
        border-bottom: 2px solid darkgray;
    }
    & > :last-child {
        border-bottom: none;
    }
`;
