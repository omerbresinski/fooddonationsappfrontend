import React, { useEffect, useMemo } from "react";
import { register } from "store/actions";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useUser, useForm } from "hooks";
import Text from "components/common/Text";
import Button from "components/common/Button";
import TextInput from "components/common/TextInput";
import SelectInput from "components/common/SelectInput";
import { BaseRegister, Form, Header, ButtonWrapper } from "./style";
import { URL } from "constant";

const Register = () => {
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const { form, handleInputChange } = useForm();
    const registrationOptions = useMemo(
        () => [
            { value: 0, label: "Player" },
            { value: 1, label: "Manager" },
            { value: 2, label: "Admin" },
        ],
        []
    );

    useEffect(() => {
        if (user) {
            history.push(URL.home);
        }
    }, [user]);

    const onRegisterClick = () => {
        dispatch(register(form, onRegisterSuccess));
    };

    const onRegisterSuccess = () => {
        history.push(URL.home);
    };

    return (
        <BaseRegister>
            <Header>
                <Text size={"82px"} color={"darkslateblue"} withStroke>
                    Register
                </Text>
            </Header>
            <Form>
                <TextInput fieldName={"username"} label={"Username"} onChange={handleInputChange} width={"400px"} value={form.username || ""} />
                <TextInput fieldName={"email"} label={"Email"} onChange={handleInputChange} width={"400px"} value={form.email || ""} />
                <TextInput fieldName={"avatar"} label={"Avatar URL"} onChange={handleInputChange} width={"400px"} value={form.avatar || ""} />
                <SelectInput
                    fieldName={"role"}
                    label={"Role"}
                    onChange={handleInputChange}
                    width={"400px"}
                    value={form.role}
                    options={registrationOptions}
                />
                <ButtonWrapper>
                    <Button onClick={onRegisterClick} value={"Submit"} size={"32px"} hoverColor={"darkslateblue"} />
                </ButtonWrapper>
            </Form>
        </BaseRegister>
    );
};

export default Register;
