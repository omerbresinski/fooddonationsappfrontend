import styled from "styled-components";

export const BaseRegister = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    padding: 150px 50px;
    align-items: center;
    box-sizing: border-box;
    gap: 50px;
`;

export const Form = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
`;

export const Header = styled.div`
    display: flex;
`;

export const RegisterSuggestion = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 5px;
`;

export const ButtonWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin-block-start: 20px;
`;
