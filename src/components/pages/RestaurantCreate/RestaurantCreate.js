import React, { useEffect } from "react";
import { createItem, setMapPreview, updateUserToManager } from "store/actions";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useUser, useForm, withAuth } from "hooks";
import { URL } from "constant";
import Text from "components/common/Text";
import Button from "components/common/Button";
import TextInput from "components/common/TextInput";
import PlacesSearchInput from "components/common/PlacesSearchInput";
import { BaseRestaurantCreate, ButtonWrapper, Form, Header } from "./style";

const RestaurantCreate = () => {
    const { user } = useUser();
    const history = useHistory();
    const dispatch = useDispatch();
    const { form, handleInputChange } = useForm();

    useEffect(() => {
        dispatch(updateUserToManager(user.userId.email));
    }, []);

    const handleSearchChange = (location) => {
        handleInputChange("location", location);
        dispatch(setMapPreview(location));
    };

    const onCreateSuccess = () => {
        history.push(URL.home);
    };

    const onCreateClick = () => {
        dispatch(createItem(user.userId?.email, form, onCreateSuccess));
    };

    return (
        <BaseRestaurantCreate>
            <Header>
                <Text size={"72px"} color={"darkslateblue"} withStroke>
                    New Food Provider
                </Text>
            </Header>
            <Form>
                <TextInput fieldName={"name"} label={"Name"} onChange={handleInputChange} width={"400px"} value={form.name || ""} />
                <PlacesSearchInput onChange={handleSearchChange} />
                <ButtonWrapper>
                    <Button onClick={onCreateClick} value={"Submit"} size={"32px"} hoverColor={"darkslateblue"} />
                </ButtonWrapper>
            </Form>
        </BaseRestaurantCreate>
    );
};

export default withAuth(React.memo(RestaurantCreate));
