export const URL = {
    home: "/",
    login: "/login",
    register: "/register",
    restaurantCreate: "/restaurantcreate",
    users: "/users",
    operations: "/operations",
    items: "/items",
};

export const ISRAEL_UTC_OFFSET = 3;

export const API_KEY = "AIzaSyBTVtcYrKvr3jmR4aP2D0Ik62ERXU_xyYU";

export const DATE_FORMAT = "d/M/YYYY HH:mm";
