import { useState, useCallback } from "react";

const useForm = () => {
    const [form, setForm] = useState({});

    const handleInputChange = useCallback((fieldName, value) => {
        setForm((form) => {
            return { ...form, [fieldName]: value };
        });
    }, []);

    return { form, handleInputChange };
};

export default useForm;
