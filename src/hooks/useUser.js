import { useSelector } from "react-redux";
import { createSelector } from "reselect";

const useUser = () => {
    const user = useSelector(selectUser);
    return { user };
};

export default useUser;

const getUsers = (state) => state.users;
const selectUser = createSelector([getUsers], (users) => users?.find((user) => user.userId?.email));
