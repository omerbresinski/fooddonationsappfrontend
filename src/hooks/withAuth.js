import { useUser } from "hooks";
import Login from "components/pages/Login";

const withAuth = (Component) => (props) => {
    const { user } = useUser();

    return user ? <Component {...props} /> : <Login />;
};

export default withAuth;
