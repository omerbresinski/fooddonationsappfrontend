export const actionTypes = {
    operations: {
        createOperationGet: "CREATE_OPERATION_GET",
        createOperationSet: "CREATE_OPERATION_SET",
        schedulePickupGet: "SCHEDULE_PICKUP_GET",
        schedulePickupSet: "SCHEDULE_PICKUP_SET",
        deliveredGet: "DELIVERED_GET",
        deliveredSet: "DELIVERED_SET",
    },
    users: {
        logout: "LOGOUT",
        loginGet: "LOGIN_GET",
        loginSet: "LOGIN_SET",
        registerGet: "REGISTER_GET",
        updateToPlayerGet: "UPDATE_TO_PLAYER_GET",
        updateToPlayerSet: "UPDATE_TO_PLAYER_SET",
        updateToManagerGet: "UPDATE_TO_MANAGER_GET",
        updateToManagerSet: "UPDATE_TO_MANAGER_SET",
    },
    items: {
        itemsGet: "ITEMS_GET",
        itemsSet: "ITEMS_SET",
        itemCreateGet: "ITEM_CREATE_GET",
        itemCreateSet: "ITEM_CREATE_SET",
        setSelected: "SET_SELECTED",
    },
    mapPreview: {
        mapPreviewSet: "MAP_PREVIEW_SET",
    },
};
