import { actionTypes } from "../actionTypes";

const ADMIN_SPACE = "2021a.amit.kremer";
const MANAGER_SPACE = "2021a.amit.kremer";

export const getAllOperations = (adminSpace, adminEmail, onGetSuccess, onFailure) => {
    return apiAction({
        url: `http://localhost:8081/dts/operations/${adminSpace}/${adminEmail}`,
        method: "GET",
        type: actionTypes.operations.getAll,
        onSuccess: (operations) => {
            onGetSuccess();
            return {
                type: actionTypes.operations.setAll,
                payload: operations,
            };
        },
        onFailure,
    });
};

export const login = (data, onLoginSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/users/login/${ADMIN_SPACE}/${data.email}`,
        method: "GET",
        type: actionTypes.users.loginGet,
        onSuccess: (user) => {
            onLoginSuccess();
            return {
                type: actionTypes.users.loginSet,
                payload: user,
            };
        },
        onFailure,
    });
};

export const updateUserToPlayer = (userEmail, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/users/${ADMIN_SPACE}/${userEmail}`,
        method: "PUT",
        type: actionTypes.users.updateToPlayerGet,
        data: {
            role: "PLAYER",
        },
        onSuccess: (user) => {
            return {
                type: actionTypes.users.updateToPlayerSet,
                payload: user,
            };
        },
        onFailure,
    });
};

export const updateUserToManager = (userEmail, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/users/${ADMIN_SPACE}/${userEmail}`,
        method: "PUT",
        type: actionTypes.users.updateToManagerGet,
        data: {
            role: "MANAGER",
        },
        onSuccess: (user) => {
            return {
                type: actionTypes.users.updateToManagerSet,
                payload: user,
            };
        },
        onFailure,
    });
};

export const logout = () => {
    return {
        type: actionTypes.users.logout,
    };
};

export const register = (data, onRegisterSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/users/`,
        method: "POST",
        data,
        type: actionTypes.users.registerGet,
        onSuccess: (user) => {
            onRegisterSuccess();
            return {
                type: actionTypes.users.loginSet,
                payload: user,
            };
        },
        onFailure,
    });
};

export const createItem = (userEmail, data, onCreateSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/items/${MANAGER_SPACE}/${userEmail}`,
        method: "POST",
        type: actionTypes.items.itemCreateGet,
        data: {
            ...data,
            active: true,
            type: "DONATOR",
        },
        onSuccess: (items) => {
            onCreateSuccess();
            return {
                type: actionTypes.items.itemCreateSet,
                payload: items,
            };
        },
        onFailure,
    });
};

export const getAllItems = (userEmail, onGetSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/items/${MANAGER_SPACE}/${userEmail}`,
        method: "GET",
        type: actionTypes.items.itemsGet,
        onSuccess: (items) => {
            onGetSuccess();
            return {
                type: actionTypes.items.itemsSet,
                payload: items,
            };
        },
        onFailure,
    });
};

export const createOperation = (data, onCreateSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/operations/`,
        method: "POST",
        type: actionTypes.operations.createOperationGet,
        data,
        onSuccess: (operation) => {
            onCreateSuccess();
            return {
                type: actionTypes.operations.createOperationSet,
                payload: { operation, operationType: data.type },
            };
        },
        onFailure,
    });
};

export const schedulePickup = (data, onScheduleSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/operations/`,
        method: "POST",
        type: actionTypes.operations.schedulePickupGet,
        data,
        onSuccess: (operation) => {
            onScheduleSuccess();
            return {
                type: actionTypes.operations.schedulePickupSet,
                payload: { operation, operationType: data.type },
            };
        },
        onFailure,
    });
};

export const setFoodDelivered = (data, onDeliveredSuccess = () => {}, onFailure = () => {}) => {
    return apiAction({
        url: `http://localhost:8081/dts/operations/`,
        method: "POST",
        type: actionTypes.operations.deliveredGet,
        data,
        onSuccess: (operation) => {
            onDeliveredSuccess();
            return {
                type: actionTypes.operations.deliveredSet,
                payload: { operation, operationType: data.type },
            };
        },
        onFailure,
    });
};

export const setSelectedItem = (item) => {
    return {
        type: actionTypes.items.setSelected,
        payload: item,
    };
};

export const setMapPreview = (preview) => {
    return {
        type: actionTypes.mapPreview.mapPreviewSet,
        payload: preview,
    };
};

const apiAction = ({ type, payload = {}, url, method, data, headers = {}, onSuccess, onFailure }) => {
    return {
        type,
        payload,
        metadata: {
            api: {
                url,
                method,
                data,
                headers,
                onSuccess,
                onFailure,
            },
        },
    };
};
