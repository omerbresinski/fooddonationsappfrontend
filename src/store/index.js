import { combineReducers } from "redux";
import items from "./reducers/items";
import users from "./reducers/users";
import mapPreview from "./reducers/mapPreview";
import operations from "./reducers/operations";

export default combineReducers({
    users,
    items,
    operations,
    mapPreview,
});
