import { actionTypes } from "../actionTypes";

const items = (state = [], action) => {
    switch (action.type) {
        case actionTypes.items.itemCreateSet:
            return [...state, action.payload];
        case actionTypes.items.itemsSet:
            return [...action.payload];
        case actionTypes.items.setSelected:
            return setSelectedItem(state, action.payload);
        case actionTypes.operations.createOperationSet:
            return setPickupWindow(state, action.payload);
        case actionTypes.operations.closePickupWindow:
            return setPickupWindow(state, action.payload);
        case actionTypes.operations.schedulePickupSet:
            return schedulePickupWindow(state, action.payload);
        case actionTypes.operations.deliveredSet:
            return setDeliveredSuccess(state, action.payload);
        default:
            return state;
    }
};

const setSelectedItem = (items, selectedItem) => {
    return items.map((item) => {
        if (item.isSelected) {
            return { ...item, isSelected: item.itemId.id === selectedItem.itemId.id };
        } else {
            if (item.itemId.id === selectedItem.itemId.id) {
                return { ...item, isSelected: true };
            }
            return item;
        }
    });
};

const setPickupWindow = (items, payload) => {
    return items.map((item) => {
        if (item.itemId.id === payload.operation.item.itemId.id) {
            const reservations = [...(item.itemAttributes.reservasionsDetails || [])];
            if (payload.operationType !== "create_pickup_window") {
                const indexToRemove = reservations.findIndex(
                    (reservation) => reservation.name === payload.operation.operationAttributes.reservasionsDetails[0].name
                );
                reservations.splice(indexToRemove, 1);
            }
            return {
                ...item,
                itemAttributes: {
                    reservasionsDetails:
                        payload.operationType === "create_pickup_window"
                            ? [...reservations, ...payload.operation.operationAttributes.reservasionsDetails]
                            : [...reservations],
                },
            };
        } else {
            return item;
        }
    });
};

const schedulePickupWindow = (items, payload) => {
    return items.map((item) => {
        if (payload.operation.item.itemId.id === item.itemId.id) {
            const currentReservationDetails = item.itemAttributes.reservasionsDetails.find(
                ({ name }) => name === payload.operation.operationAttributes.name
            );
            currentReservationDetails.recievers.push(payload.operation.operationAttributes);
            return { ...item };
        } else {
            return item;
        }
    });
};

const setDeliveredSuccess = (items, payload) => {
    return items.map((item) => {
        if (payload.operation.item.itemId.id === item.itemId.id) {
            const amountToDeduct = +item.itemAttributes.reservasionsDetails[0].recievers[0].foodsToTake[0].numberOfDishes;
            item.itemAttributes.reservasionsDetails[0].foodsToGive[0].numberOfDishes -= amountToDeduct;
            item.itemAttributes.reservasionsDetails[0].recievers = undefined;
            return { ...item };
        } else {
            return item;
        }
    });
};

export default items;
