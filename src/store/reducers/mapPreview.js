import { actionTypes } from "../actionTypes";

const mapPreview = (state = [], action) => {
    switch (action.type) {
        case actionTypes.mapPreview.mapPreviewSet:
            return [action.payload];
        default:
            return state;
    }
};

export default mapPreview;
