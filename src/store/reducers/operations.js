import { actionTypes } from "../actionTypes";

const operations = (state = [], action) => {
    switch (action.type) {
        case actionTypes.operations.createOperationSet:
            return [...state, action.payload];
        default:
            return state;
    }
};

export default operations;
