import { actionTypes } from "../actionTypes";

const users = (state = [], action) => {
    switch (action.type) {
        case actionTypes.users.loginSet:
            return [...state, action.payload];
        case actionTypes.users.logout:
            return [];
        default:
            return state;
    }
};

export default users;
